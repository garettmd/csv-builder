import os

from flask import Flask, render_template, redirect, url_for
from flask_bootstrap import Bootstrap
from raven.contrib.flask import Sentry
from werkzeug.exceptions import NotFound, HTTPException

csvbuilder = Flask(__name__)
bootstrap = Bootstrap(csvbuilder)
sentry = Sentry(csvbuilder,
                dsn='https://ea8882921c084360b422ab4b55e28893:0ef8c0bf008540c4b8d9f006f7b13462@sentry.io/279450')

if os.environ.get('CSV_BUILDER_TYPE') == 'Production':
    csvbuilder.config.from_object('config.ProductionConfig')
    sentry.tags_context({'environment': 'Production'})
elif os.environ.get('CSV_BUILDER_TYPE') == 'Development':
    from flask_debugtoolbar import DebugToolbarExtension

    csvbuilder.config.from_object('config.DevelopmentConfig')
    toolbar = DebugToolbarExtension(csvbuilder)
    sentry.tags_context({'environment': 'Development'})

from csvbuilder import views





@csvbuilder.errorhandler(NotFound)
def not_found(error):
    return render_template('404.html'), 404



