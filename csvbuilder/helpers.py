import csv
import os
import re

import openpyxl
from flask import flash, redirect, url_for, render_template, send_file
from validus import ismac
import git
import zipfile

from csvbuilder import csvbuilder

EQUIPMENT_LIST = csvbuilder.config['EQUIPMENT_LIST']
INSTANCE_TYPES = csvbuilder.config['INSTANCE_TYPES']
SKIP_UNITS = csvbuilder.config['SKIP_UNITS']
NET_LIST = csvbuilder.config['NETWORK_LIST']
ALLOWED_EXTENSIONS = csvbuilder.config['ALLOWED_EXTENSIONS']
REPO_DIR = csvbuilder.config['SITE_INV_REPO_DIR']
TEMPLATE_DIR = csvbuilder.config['TEMPLATE_DIR']
TEMP_DEFAULTS = csvbuilder.config['TEMPLATE_DEFAULTS']


class BadRackName(Exception):
    pass


class BadDescription(Exception):
    pass


class BadMacAddress(Exception):
    pass


class BadRackPosition(Exception):
    pass


class BadEquipment(Exception):
    pass


class MissingFormObject(Exception):
    pass


@csvbuilder.errorhandler(BadRackName)
def redirect_on_bad_rack_name(e):
    print(f'Error: {e}')
    print('bad rack name error')
    return render_template('error.html', errors=e)


@csvbuilder.errorhandler(BadDescription)
def redirect_on_bad_description(e):
    print(f'Error: {e}')
    print('bad description error')
    return render_template('error.html', errors=e)


@csvbuilder.errorhandler(BadMacAddress)
def redirect_on_bad_mac_address(e):
    print(f'Error: {e}')
    print('bad mac address error')
    return render_template('error.html', errors=e)


@csvbuilder.errorhandler(BadRackPosition)
def redirect_on_bad_rack_position(e):
    print(f'Error: {e}')
    print('bad rack position error')
    return render_template('error.html', errors=e)


@csvbuilder.errorhandler(BadEquipment)
def redirect_on_bad_equipment(e):
    print(f'Error: {e}')
    print('bad equipment error')
    return render_template('error.html', errors=e)


@csvbuilder.errorhandler(MissingFormObject)
def redirect_on_missing_form_object(e):
    print('missing object error')
    return render_template('error.html', errors=e)


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def process_racksheet(file):
    wb = openpyxl.load_workbook(file)
    try:
        ws = wb['Rack Sheet']
    except KeyError:
        ws = wb[wb.sheetnames[0]]
    rack_reg = re.compile('\(+[A-Za-z]{1,2}\.*[0-9]{2,3}\)+$')  # standard naming
    rack_allnum_reg = re.compile('\([0-9.:]+\)$')  # lite site naming
    rack_she_reg = re.compile('\([A-Za-z0-9]*-[0-9]*\)$')  # SHE site naming
    d6 = ws['d6'].value.lower().strip()  # Grab cell where rack name is listed
    rack = re.search(rack_reg, d6)
    if not rack:
        rack = re.search(rack_allnum_reg, d6)
        if not rack:
            rack = re.search(rack_she_reg, d6)
            if not rack:
                flash(f'Could not determine rack name from file {file}', 'error')
                raise BadRackName
    rack = rack.group().strip('(').strip(')').replace('.', 'X').replace('-', 'X').replace(':', 'X').upper()
    values = {}
    for idx, row in enumerate(ws['b13:h100']):
        if 'build & test' in str(row[0].value).lower():
            break
        else:
            values[idx] = []
            for cell in row:
                if cell.value:
                    values[idx].append(cell.value)
                else:
                    values[idx].append('')

    return values, rack


def sort_inventory(values, site_code, rack_location, region):
    sorted_values = []
    for idx, row in values.items():
        row0 = re.sub(r'[Ss]witch|\(|\)', '', row[0]).strip()
        if row0 == 'build & test completed':
            break
        if row0 in EQUIPMENT_LIST and not any(unit in row[6].lower() for unit in SKIP_UNITS):
            equipment = row0

            instance_type, abbreviation, description = get_instance_type(idx, row, region)

            service_tag = get_service_tag(row)

            mac_address = get_mac_address(idx, row)

            pos_top, pos_bottom = get_rack_positions(idx, row)

            node_name = compile_node_name(abbreviation, site_code, rack_location, pos_bottom)

            sorted_values.append(
                [rack_location, pos_bottom, pos_top, equipment, service_tag, node_name, mac_address, instance_type,
                 description])

    if not sorted_values:
        raise BadEquipment
    else:
        return sorted_values


def get_instance_type(idx, row, region):
    try:
        description = row[6].lower().strip()
    except IndexError:
        flash(f'Missing description on row {idx}. Please correct then try again', 'error')
        raise BadDescription(f'Missing description on row {idx}. Please correct then try again')
    try:
        instance_type, abbreviation = INSTANCE_TYPES[re.sub(r'\d|stack', '', description).strip()]
    except KeyError:
        raise BadDescription(f'Invalid description for device {row[0]}, description {row[6]}, RU {row[4]}')

    return instance_type, abbreviation, description


def get_equipment(idx, row):
    if row[0] in EQUIPMENT_LIST:
        equipment = row[0]
        return equipment
    else:
        flash(f'Invalid description of device on row {idx}. Please correct then try again.')
        raise BadEquipment


def get_service_tag(row):
    try:
        service_tag = row[1]
    except IndexError:
        service_tag = ''

    return service_tag


def get_mac_address(idx, row):
    if row[3]:
        if ismac(row[3]):
            mac_address = row[3].upper()
        else:
            flash(f'Invalid MAC address on row {idx}. Please correct then try again', 'error')
            raise BadMacAddress
    else:
        mac_address = ''

    return mac_address


def get_rack_positions(idx, row):
    try:
        if '-' in row[4]:
            positions = str(row[4]).split('-')
            pos_top = positions[1].lstrip('0')
            pos_bottom = positions[0].lstrip('0')
        else:
            pos_top = row[4].lstrip('0')
            pos_bottom = pos_top
    except AttributeError:
        flash(f'There\'s some weird formatting on the RU level on row {idx}. Please check and try again', 'error')
        raise BadRackPosition

    return pos_top, pos_bottom


def compile_node_name(abbreviation, site_code, rack_location, pos_bottom):
    node_name = f'CR{abbreviation}{site_code}{rack_location}-{pos_bottom}'

    return node_name


def generate_files(final):
    region = final.pop('region')[0]
    site = final.pop('site')[0]
    site_type = final.pop('site_type')[0]
    site_code = final.pop('site_code')[0]
    arch = final.pop('arch')[0]
    nrack1 = final.pop('nrack1')[0]
    nrack2 = final.pop('nrack2')[0]
    racks = set(rack for rack in final['rack[]'] if rack not in [nrack1, nrack2])
    region_mod = (int(re.findall(r'\d', region)[0]) - 1) % 4 + 1
    region_site = f'{region}-{site}'
    index = range(0, len(final['rack[]']))
    values = []
    files = {}
    for unit in index:
        values.append([
            final['rack[]'][unit],
            final['rack_position_bottom[]'][unit],
            final['rack_position_top[]'][unit],
            final['equipment[]'][unit],
            final['service_tag[]'][unit],
            final['node_name[]'][unit],
            final['mac_address[]'][unit],
            final['instance_type[]'][unit],
            final['description[]'][unit],
        ])

    if site_type in ['core_1.2', 'core_1.1']:
        files['compute'] = []
        files['network'] = []
        values = clean_bigswitches(values, region_mod)

        invnet_removes, new_file = (generate_invnet(values, region_mod, region_site))
        for unit in reversed(invnet_removes):
            values.pop(unit)
        files['compute'].append(new_file)

        invemc_removes, new_file = generate_invemc(values, region_site)
        for unit in reversed(invemc_removes):
            values.pop(unit)
        files['compute'].append(new_file)

        emc_values, new_file = generate_bigswitch(values, region_site, site_type, region_mod=region_mod, nrack1=nrack1,
                                        nrack2=nrack2, bigemc=True)
        files['compute'].append(new_file)
        new_file = generate_bigemc(emc_values, region_site)
        files['compute'].append(new_file)

        network_removes, new_file = generate_network(values, region, site)
        for unit in reversed(network_removes):
            values.pop(unit)
        files['network'].append(new_file)

        new_file = generate_inventory(values, region_site)
        files['compute'].append(new_file)
        new_file = generate_torswitch(values, region_site, site_type)
        files['compute'].append(new_file)
    elif site_type in ['lite-non-nebs', 'storage', 'monitoring']:
        files['standard'] = []
        new_file = generate_inventory(values, region_site)
        files['standard'].append(new_file)
        new_file = generate_bigswitch(values, region_site, site_type=site_type, nrack1=nrack1, nrack2=nrack2)
        files['standard'].append(new_file)
        new_file = generate_torswitch(values, region_site, site_type)
        files['standard'].append(new_file)
    remove_empties(region_site, 'bigswitch', nrack1=nrack1, nrack2=nrack2)
    remove_empties(region_site, 'torswitch')

    return files


def generate_invnet(values, region_mod, region_site):
    invnet_values = []
    inventory_removes = []
    for num, unit in enumerate(list(values)):
        if 'sdn controller' in unit[8]:
            if f'stack {region_mod}' in unit[8]:
                invnet_values.append(unit)
            inventory_removes.append(num)
        elif 'spine' in unit[8]:
            if f'stack {region_mod}' in unit[8]:
                invnet_values.append(unit)
            inventory_removes.append(num)
        elif 'service leaf' in unit[8]:
            if f'stack {region_mod}' in unit[8]:
                invnet_values.append(unit)
            inventory_removes.append(num)
    invnet_values.sort(reverse=True, key=lambda x: (x[0], x[2]))
    invnet_file = overwrite_csv(invnet_values, region_site, 'invnet')

    return inventory_removes, invnet_file


def generate_bigswitch(values, region_site, site_type, **kwargs):
    racks = sorted(set(unit[0] for unit in values))
    nracks = []
    if 'nrack1' in kwargs:
        nr1 = kwargs.pop('nrack1')
        nracks.append(nr1)
        if nr1 in racks:
            racks.remove(nr1)
    if 'nrack2' in kwargs:
        nr2 = kwargs.pop('nrack2')
        nracks.append(nr2)
        if nr2 in racks:
            racks.remove(nr2)
    if 'region_mod' in kwargs:
        template = os.path.join(TEMPLATE_DIR, f'bigswitch_{kwargs["region_mod"]}_{site_type}.csv')
    else:
        template = os.path.join(TEMPLATE_DIR, f'bigswitch_{site_type}.csv')
    with open(template, 'r') as temp:
        temp_values = []
        for line in temp:
            if line[0] == '#':
                continue
            for num, default in enumerate(TEMP_DEFAULTS['regular']):
                try:
                    line = line.replace(default, racks[num]).replace('\n', '')
                except IndexError:
                    pass
            for nnum, ndefault in enumerate(TEMP_DEFAULTS['network']):
                try:
                    line = line.replace(ndefault, nracks[nnum]).replace('\n', '')
                except IndexError:
                    pass
            new_line = line.split(',')
            temp_values.append(new_line)
    if 'bigemc' in kwargs:
        emc_values = []
        new_values = []
        for line in temp_values:
            if line[0] == 'STORAGE':
                emc_values.append(line)
            else:
                new_values.append(line)
        bigswitch_file = overwrite_csv(new_values, region_site, 'bigswitch')
        return emc_values, bigswitch_file
    else:
        bigswitch_file = overwrite_csv(temp_values, region_site, 'bigswitch')
        return bigswitch_file


def generate_invemc(values, region_site):
    invemc_values = []
    inventory_removes = []
    for num, line in enumerate(values):
        if line[8] == 'storage leaf':
            invemc_values.append(line)
            inventory_removes.append(num)
    invemc_values.sort(reverse=True, key=lambda x: (x[0], x[2]))
    invemc_file = overwrite_csv(invemc_values, region_site, 'invemc')

    return inventory_removes, invemc_file


def generate_bigemc(final, region_site):
    bigemc_file = overwrite_csv(final, region_site, 'bigemc')

    return bigemc_file


def generate_torswitch(values, region_site, site_type):
    temp_values = []
    racks = sorted(set(unit[0] for unit in values))
    template = os.path.join(TEMPLATE_DIR, f'torswitch_{site_type}.csv')
    with open(template, 'r') as temp:
        for line in temp:
            if line[0] == '#':
                continue
            for num, default in enumerate(TEMP_DEFAULTS['regular']):
                try:
                    line = line.replace(default, racks[num]).replace('\n', '')
                except IndexError:
                    pass
            new_line = line.split(',')
            temp_values.append(new_line)
    torswitch_file = overwrite_csv(temp_values, region_site, 'torswitch')

    return torswitch_file


def generate_inventory(values, region_site):
    inventory_file = overwrite_csv(values, region_site, 'inventory')

    return inventory_file


def clean_bigswitches(values, region_mod):
    extra_bigswitches = []
    for idx, line in enumerate(values):
        try:
            if line[7] == 'BIGSWITCH':
                if f'stack {region_mod}' not in line[8]:
                    extra_bigswitches.append(idx)
            elif line[7] == 'SPINE':
                if f'stack {region_mod}' not in line[8]:
                    extra_bigswitches.append(idx)
            elif 'service leaf' in line[8]:
                if f'stack {region_mod}' not in line[8]:
                    extra_bigswitches.append(idx)
        except IndexError:
            continue

    for unit in reversed(extra_bigswitches):
        del values[unit]

    return values


def generate_network(values, region, site):
    network_inventory = []
    network_removes = []
    for num, line in enumerate(values):
        if re.sub(r'\d|stack', '', line[8]).strip() in NET_LIST:
            network_inventory.append(line)
            network_removes.append(num)
    region_num = int(re.findall('\d+', region)[0])
    pod = get_pod(region_num)
    pod_site = f'n{pod}-{site}'
    network_file = overwrite_csv(network_inventory, pod_site, 'inventory')

    return network_removes, network_file


def get_pod(region):
    counter = 1
    for x in range(1, region):
        if x % 4 == 0:
            counter += 1
    return counter


def remove_empties(region_site, site_type, **kwargs):
    nracks = set()
    if 'nrack1' in kwargs:
        nracks.add(kwargs['nrack1'])
    if 'nrack2' in kwargs:
        nracks.add(kwargs['nrack2'])
    inventory = os.path.join(REPO_DIR, 'sites', f'{region_site}', 'inventory.csv')
    filepath = os.path.join(REPO_DIR, 'sites', f'{region_site}', f'{site_type}.csv')
    with open(inventory, 'r') as invfile:
        inv_data = [line for line in csv.reader(invfile)]
    rus = set(f'{row[0]}:{row[1]}' for row in inv_data)
    with open(filepath, 'r') as typefile:
        file_data = [line for line in csv.reader(typefile)]
    with open(filepath, 'w') as typefile:
        writer = csv.writer(typefile)
        writer.writerow(csvbuilder.config['CSV_HEADERS'][site_type])
        for row in file_data:
            if f'{row[4]}:{row[5]}' in rus or row[4] in nracks or 'BAGG' in row[0]:
                writer.writerow(row)

    return


def overwrite_csv(values, region_site, site_type):
    v = list(values)
    filepath = os.path.join(REPO_DIR, 'sites', region_site)
    if not os.path.exists(filepath):
        os.mkdir(filepath)
    csv_file = os.path.join(filepath, f'{site_type}.csv')
    with open(csv_file, 'w') as file:
        file.truncate()
        writer = csv.writer(file)
        writer.writerow(csvbuilder.config['CSV_HEADERS'][site_type])
        if site_type in ['inventory', 'invemc', 'invnet']:
            for row in v:
                writer.writerow([cell for cell in row[:-1]])
        else:
            for row in v:
                writer.writerow(row)
    # flash(f'{site_type} file for {region_site} generated successfully', 'success')
    return csv_file


def debug():
    assert csvbuilder.debug is False, 'debug requested'
    return


def zip_files(files, region_site):
    zips = set()
    region, site = region_site.split('-')
    region_num = int(re.findall('\d+', region)[0])
    for rack_type, file_list in files.items():
        if rack_type == 'network':
            network_num = get_pod(region_num)
            net_region_site = f'n{network_num}-{site}'
            net_zip_file = os.path.join(REPO_DIR, 'sites', net_region_site, f'{net_region_site}.zip')
            zips.add(net_zip_file[1:])
            with zipfile.ZipFile(net_zip_file, 'w') as nz:
                for file in file_list:
                    nz.write(file, os.path.basename(file))
        else:
            zip_file = os.path.join(REPO_DIR, 'sites', region_site, f'{region_site}.zip')
            zips.add(zip_file[1:])
            with zipfile.ZipFile(zip_file, 'w') as z:
                for file in file_list:
                    z.write(file, os.path.basename(file))

    return zips


def send_files(files):
    for file in files:
        send_file(file, as_attachment=True, attachment_filename=os.path.basename(file))

    return


def get_git_status():
    repo = git.Repo(REPO_DIR)
    new = repo.untracked_files  # Get new files
    modified = []
    sites = set()
    files = {}
    for m in repo.index.diff(None).iter_change_type('M'):  # Get modified files
        modified.append(m.b_path)
    for f in modified:
        f_split = f.split('/')
        for directory in f_split:
            if directory == 'sites':
                site = f_split[f_split.index(directory) + 1]
                if site not in files:
                    files[site] = {}
                if 'modified' not in files[site]:
                    files[site]['modified'] = []
                files[site]['modified'].append(f)

    for f in new:
        f_split = f.split('/')
        for directory in f_split:
            if directory == 'sites':
                site = f_split[f_split.index(directory) + 1]
                if site not in files:
                    files[site] = {}
                if 'new' not in files[site]:
                    files[site]['new'] = []
                files[site]['new'].append(f)
    return files