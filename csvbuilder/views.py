import os
import re

from flask import render_template, url_for, redirect, request, flash, session, send_file
from werkzeug.utils import secure_filename

from csvbuilder import csvbuilder
from csvbuilder.helpers import (allowed_file, process_racksheet, sort_inventory, generate_files,
                                clean_bigswitches, zip_files)

UPLOAD_DIR = csvbuilder.config['UPLOAD_DIR']


@csvbuilder.route('/')
def base():
    return redirect(url_for('welcome'))


@csvbuilder.route('/welcome')
def welcome():
    return render_template('main.html')


@csvbuilder.route('/inventory')
def inventory():
    session.pop('_flashes', None)
    return render_template('inventory.html')


@csvbuilder.route('/inventory/upload', methods=['POST'])
def inventory_upload():
    # session.pop('_flashes', None)
    if 'file[]' not in request.files:
        flash('No file selected', 'error')
        return redirect(request.url)
    files = request.files.getlist('file[]')
    site = request.form['site'].lower()
    site_type = request.form['site_type'].lower()
    site_code = request.form['site_code'].upper()
    region = request.form.get('region', '').lower()
    nrack1 = request.form.get('nrack1', '').upper()
    nrack2 = request.form.get('nrack2', '').upper()
    values = []
    file_list = []
    for file in files:
        if allowed_file(file.filename):
            filename = secure_filename(file.filename)
            filepath = os.path.join(UPLOAD_DIR, filename)
            file.save(filepath)
            file_values, rack = process_racksheet(file)
            file_list.append(filename)
            values.append(sort_inventory(file_values, site_code, rack, region))
            values = clean_bigswitches(values, region)
        else:
            flash(f'{file.filename} is not a valid xls or xlsx file', 'warning')
    return render_template('inventory_review.html', values=values, site=site, files=file_list, region=region,
                           site_type=site_type, site_code=site_code, nrack1=nrack1, nrack2=nrack2)


@csvbuilder.route('/inventory/save', methods=['POST'])
def inventory_save():
    session.pop('_flashes', None)
    final = request.form.to_dict(flat=False)
    region = final['region'][0]
    site = final['site'][0]
    region_site = f'{region}-{site}'
    region_mod = (int(re.findall(r'\d', region)[0]) - 1) % 4 + 1
    files = generate_files(final)
    zips = zip_files(files, region_site)
    names = [os.path.basename(file) for file in zips]
    flash('csv files successfully generated', 'success')
    return render_template('download.html', files=zips, names=names)


@csvbuilder.route('/download/<path:file>')
def download_file(file):
    file = f'/{file}'
    return send_file(file, as_attachment=True, attachment_filename=os.path.basename(file))
