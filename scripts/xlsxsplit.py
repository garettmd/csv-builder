#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# Use this script to break out worksheets in a single workbook into several workbooks with one worksheet each
# This is useful for getting the 'Rack Sheet' sheet from a workbook
# Would be good to add an argument to specify a specific worksheet you're looking for, rather than grabbing every sheet

import argparse
import openpyxl

parser = argparse.ArgumentParser()
parser.add_argument('input')
parser.add_argument('output')
args = parser.parse_args()

infile = args.input
output = args.output


def split(infile, output):
    wb = openpyxl.load_workbook(infile)
    for old_sheet_name in wb.get_sheet_names():
        rows = []
        new = openpyxl.Workbook()
        old_sheet = wb.get_sheet_by_name(old_sheet_name)
        for row in old_sheet.iter_rows():
            row_data = []
            for cell in row:
                row_data.append(cell.value)
            rows.append(row_data)
        new.create_sheet('Rack Sheet')
        new_sheet = new.get_sheet_by_name('Rack Sheet')
        for row in rows:
            new_sheet.append(row)
        try:
            new.remove_sheet(new.get_sheet_by_name('Sheet'))
        except ValueError:
            pass
        new.template = False
        new.save('{}{}.xlsx'.format(output, old_sheet_name))


if __name__ == '__main__':
    split(infile, output)
