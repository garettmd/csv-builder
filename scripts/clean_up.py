#!/usr/bin/env python
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
#
# Use this script to fix common errors in the racksheets from OnX
# Current fixes:
# Fix semicolon in MAC address
# TOR Switch to just TOR
# Remove 1Gb from HP 5900
# Remove SKU
# Change multiple RU units to list only the bottom RU

import argparse

import openpyxl

parser = argparse.ArgumentParser()
parser.add_argument('file', nargs='*', help='Space separated list of files to update')
args = parser.parse_args()

files = args.file

'''
Racksheet help
row[0] = System Type (HP 5900, Dell R630, etc.)
row[1] = Serial number
row[2] = HostID (usually blank)
row[3] = MAC Address
row[4] = RU
row[5] = System (usually blank)
row[6] = Notes (specifies what the unit is used for)
'''


for file in files:
    if 'xls' in file.split('.')[-1]:
        wb = openpyxl.load_workbook(file)
        try:
            ws = wb['Rack Sheet']
        except KeyError:
            ws = wb[wb.sheetnames[0]]
        for row in ws['b13:h100']:
            if 'build & test completed' in row[0].value.lower():
                break
            try:
                if ';' in row[3].value:
                    row[3].value = row[3].value.replace(';', ':')
                if ':' not in row[3].value:
                    print(f'no : found in {row} in file {file}')
                    new_row3 = ''
                    for pair in zip(*[iter(row[3].value)]*2):
                        new_row3 + pair + ':'
                    row[3].value = new_row3[:-1]
                if 'TOR Switch' in row[6].value:
                    row[6].value = 'TOR'
                if 'HP 5900 (1Gb)' in row[0].value:
                    row[0].value = 'HP 5900'
                if 'Dell R630 - SKU#1' in row[0].value:
                    row[0].value = 'Dell R630'
                if '-' in row[4].value:
                    row[4].value = row[4].value.split('-')[1]
            except TypeError:
                continue
        wb.save(file)
