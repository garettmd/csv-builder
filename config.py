import os
from collections import namedtuple


class Config(object):
    BASE_DIR = os.path.abspath(os.path.dirname(__file__))
    THREADS_PER_PAGE = 2
    CSRF_ENABLED = True
    CSRF_SESSION_KEY = 'something-unique'
    SECRET_KEY = 'something-unique'
    ALLOWED_EXTENSIONS = (['xls', 'xlsx'])
    UPLOAD_DIR = 'uploads/'
    CSV_DIR = 'csv/'
    TEMPLATE_DIR = 'templates/'
    ADMIN_EMAIL = 'garett.dunn@example.com'

    unit_type = namedtuple('unit', ['type', 'abbreviation'])

    SKIP_UNITS = (
        'patch panel',
        'sts',
        'empty',
        'fuse panel',
        'console port patch panel',
        'static transfer switch',
        'redhat capsul'
    )

    INSTANCE_TYPES = {
        'tor': unit_type('TOR', 'TR'),
        'admin leaf': unit_type('LEAF', 'SW'),
        'tenant leaf': unit_type('LEAF', 'SW'),
        'storage leaf': unit_type('LEAF', 'SW'),
        'leaf': unit_type('LEAF', 'SW'),
        'compute server': unit_type('COMPUTE', 'BE'),
        'ceph osd server': unit_type('CEPHOSD', 'BE'),
        'site mgmt': unit_type('SITE', 'SW'),
        'edge router': unit_type('EDGE', 'RT'),
        'agg mgmt': unit_type('AGG', 'AG'),
        'sdn controller': unit_type('BIGSWITCH', 'BS'),
        'storage': unit_type('STORAGE', 'BE'),
        'terminal server': unit_type('TERMSERVER', 'TS'),
        'spine': unit_type('SPINE', 'SW'),
        'service leaf': unit_type('LEAF', 'SW'),
        'controller node': unit_type('COMPUTE', 'BE'),
        'monitoring': unit_type('HYPERMON', 'BE'),
        'edge aggregation': unit_type('AGG', 'AG'),
    }

    EQUIPMENT_LIST = (
        'Dell R630',
        'Dell R640',
        'Dell R430',
        'HP 5900',
        'Dell R630',
        'Dell R430',
        'Dell S4048',
        'Dell S6000',
        'Dell S6010',
        'Cisco 2901',
        'Cisco 2900',
        'Cisco 4331',
        'HP 5930',
        'HP 5900',
        'Netapp 2552',
        'Netapp A300',
        'Arista 7508R',
        'HP 3PAR 8450',
        'HP 3PAR Service Processor',
        'EMC 5800',
        'Dell MD3460',
    )

    NETWORK_LIST = (
        'terminal server',
        'site mgmt',
        'edge router',
        'agg mgmt',
        'edge aggregation',
    )

    CSV_HEADERS = {
        'inventory': ['#rack', 'rack_position_bottom', 'rack_position_top', 'equipment', 'service_tag', 'node_name',
                      'mac_address', 'instance_type'],
        'bigswitch': ['#fabric', 'fromrack', 'fromrackunit', 'fromport', 'torack', 'torackunit', 'toport'],
        'bigemc': ['#fabric', 'fromrack', 'fromrackunit', 'fromport', 'torack', 'torackunit', 'toport'],
        'torswitch': ['#fabric', 'fromrack', 'fromrackunit', 'fromport', 'torack', 'torackunit', 'toport'],
        'invnet': ['#rack', 'rack_position_bottom', 'rack_position_top', 'equipment', 'service_tag', 'node_name',
                   'mac_address', 'instance_type'],
        'invemc': ['#rack', 'rack_position_bottom', 'rack_position_top', 'equipment', 'service_tag', 'node_name',
                   'mac_address', 'instance_type'],
    }

    TEMPLATE_DEFAULTS = {
        'network': (
            'S06',
            'S07',
        ),
        'regular': (
            'S02',
            'S03',
            'S04',
            'S05',
        )
    }


class ProductionConfig(Config):
    DEBUG = False
    if os.environ.get('SITE_INV_REPO_DIR'):
        SITE_INV_REPO_DIR = os.environ.get('SITE_INV_REPO_DIR')
    else:
        SITE_INV_REPO_DIR = '/home/ubuntu/site_inventory/'


class DevelopmentConfig(Config):
    DEBUG = True
    PORT = os.environ.get('CSV_PORT', 5000)
    DEBUG_TB_INTERCEPT_REDIRECTS = False
    DEBUG_TB_PROFILER_ENABLED = True
    if os.environ.get('SITE_INV_REPO_DIR'):
        SITE_INV_REPO_DIR = os.environ.get('SITE_INV_REPO_DIR')
    else:
        SITE_INV_REPO_DIR = '/Users/dunnga/vcp/test_site_inventory'
